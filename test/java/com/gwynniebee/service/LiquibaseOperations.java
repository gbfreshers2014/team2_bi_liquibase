/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.service;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.iohelpers.IOGBUtils;
import com.gwynniebee.test.helper.liquibase.LiquibaseHelper;

/**
 * Liquibase operation class.
 * @author karan
 */
public class LiquibaseOperations {
    public static final Logger LOG = LoggerFactory.getLogger(LiquibaseOperations.class);

    private static LiquibaseHelper liquiHelper;

    static {
        try {
            initLiquibaseHelper();
        } catch (Exception e) {
            LOG.error("Unable to initialize Liquibase");
        }
    }

    private static void initLiquibaseHelper() throws Exception {

        Properties props = IOGBUtils.readPropertiesFromFile(new File("configuration/local/service.db.properties"));
        String driverName = props.getProperty("driver");
        String mysqlUrl = props.getProperty("url");
        String mysqlUsername = props.getProperty("username");
        String mysqlPassword = props.getProperty("password");
        String changelogFile = props.getProperty("changeLogFile");

        // Load the JDBC driver
        Class.forName(driverName);

        liquiHelper =
                new LiquibaseHelper(("--driver=" + driverName + " --changeLogFile=" + changelogFile + " --url=" + mysqlUrl + " --username="
                        + mysqlUsername + " --password=" + mysqlPassword + " update").split("\\s"));

        // NOTE: this would only be restarted once as it is static to class
        liquiHelper.killAllConnectionsOfMySQLServer(null);
    }

    public static Connection createConnection() throws SQLException {
        return liquiHelper.createConnection();
    }

    public static void createDatabase() throws Exception {
        liquiHelper.createDatabase();
    }

    public static int createSchemaThroughChangeSet() throws Exception {
        return liquiHelper.createSchemaThroughChangeSet();
    }

    public static void dropDatabase() throws Exception {
        liquiHelper.dropDatabase();
    }

    public static void completeLiquibaseReset() throws Exception {
        dropDatabase();
        createDatabase();
        createSchemaThroughChangeSet();
    }

    public static void cleanTables() throws Exception {
        // clear all data for each test
        liquiHelper.runSqlStatement("DELETE FROM Address");
        liquiHelper.runSqlStatement("DELETE FROM Employee_info");
        liquiHelper.runSqlStatement("DELETE FROM Leave_table");
        liquiHelper.runSqlStatement("DELETE FROM authentication");
        liquiHelper.runSqlStatement("DELETE FROM stats");    
    
    }

    public static LiquibaseHelper getLiquiHelper() {
        return liquiHelper;
    }
}
